import Foundation
import CoreImage

public class Session : NSObject {
	public var currentFlow = Flow()
	public var stats : [FlowActStat: CGFloat] = [.hand: 0,
												 .moral: 0,
												 .science: 0,
												 .unknown: 0,
												 .light: 0,
												 .bigfan: 0,
												 .melee: 0 ]
	public var performedPath : [Flow] = []
	public var countdownTime: Int = 0
}
