import UIKit
import Foundation
import CoreGraphics
import CoreImage
import PlaygroundSupport

public enum FlowActStat: String {
	case hand
	case moral
	case science
	case light
	case bigfan
	case melee
	case unknown = "????"
}

// -- FlowAct Param Guideliness
//
// how i embase my analysis is a secret, you wont know by looking at here. muahahaha!
// hand: CGFloat
// moral: CGFloat
// science: CGFloat
// ????: CGFloat
// light: CGFloat
// bigfan: CGFloat
// melee: CGFloat

public class FlowAct : NSObject {
	public var range: NSRange?
	public var segueID: String?
	public var attributes: [FlowActStat:Any] = [:]
	
	public init(_ range: NSRange, segueID: String, attributes: [FlowActStat:Any]) {
		super.init()
		
		self.range = range
		self.segueID = segueID
		self.attributes = attributes
	}
	
	public func formatedAttributes () -> [NSAttributedStringKey: Any]{
		var mergedDictionary : [NSAttributedStringKey: Any] = [NSAttributedStringKey(rawValue:"segueID"): self.segueID ?? "", NSAttributedStringKey.font: boldFont]
		
		self.attributes.forEach({ (key, value) in
			mergedDictionary[NSAttributedStringKey(rawValue: key.rawValue)] = value
		})
		
		return mergedDictionary
	}
}
