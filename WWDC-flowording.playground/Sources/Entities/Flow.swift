import UIKit
import Foundation
import CoreGraphics
import CoreImage
import PlaygroundSupport

// -- Flow Param Guideliness
//
// ID = Base name for the Flow
// BaseString = The base String of the Flow
// FontColor = The base color of the Flow
// BackgroundColor = The color which background image should be filtered with CIFilter
// FlowActs = Array of FlowActs for each interactible word
//

public class Flow : NSObject {
	
	fileprivate var baseString: String?
	fileprivate var flowActs : [FlowAct]?
	
	public var id: String?
	public var attributedText: NSMutableAttributedString?
	public var attributedTitle: NSAttributedString?
	public var fontColor: UIColor?
	public var backgroundColor: UIColor?
	
	public init (params: [String: Any?]) {
		super.init()
		params.forEach { (key, value) in
			switch key {
			case "ID":
				self.id = value as? String
			case "BaseString":
				self.baseString = value as? String
			case "FontColor":
				self.fontColor = value as? UIColor
			case "BackgroundColor":
				self.backgroundColor = value as? UIColor
			case "FlowActs":
				self.flowActs = value as? [FlowAct]
			default:
				break
			}
		}
		
		let attributes : [NSAttributedStringKey: Any] = [NSAttributedStringKey.font:defaultFont]
		
		let attributesBold : [NSAttributedStringKey: Any] = [NSAttributedStringKey.font:boldFont]
		
		if let baseString = self.baseString {
			let attributedText = NSMutableAttributedString(string: baseString, attributes: attributes)
			
			self.flowActs?.forEach({ (flowAct) in
				attributedText.addAttributes(flowAct.formatedAttributes(), range: flowAct.range!)
			})
			
			self.attributedText = attributedText
		}
		
		if let id = self.id {
			let attributedTitle = NSAttributedString(string: id, attributes: attributesBold)
			
			self.attributedTitle = attributedTitle
		}
		
	}
	
	public override init() {
		super.init()
		self.attributedText = NSMutableAttributedString()
	}
}

public class FlowEnding : Flow {
	
	public override init (params: [String: Any?]) {
		super.init(params: params)
		
		var attributes : [NSAttributedStringKey: Any] = [NSAttributedStringKey.font:defaultFont]
		var boldAttributes : [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: boldFont, NSAttributedStringKey(rawValue:"bite the dust"): true]
		
		if let fontColor = self.fontColor {
			attributes[NSAttributedStringKey.strokeColor] = fontColor
			boldAttributes[NSAttributedStringKey.strokeColor] = fontColor
		}
		
		if let attributedText = self.attributedText {
			let laterMesssage = NSMutableAttributedString(string: "\n\nYou've reached the end of this experience. Most texts were based off from en.wikipedia. You can change the setup options in the SetupOptions struct or rather you could simply ", attributes: attributes)
			
			let tryAgain = NSMutableAttributedString(string: "try again.", attributes: boldAttributes)
			
			laterMesssage.append(tryAgain)
			attributedText.append(laterMesssage)
			
			self.attributedText = attributedText
		}
		
		if let id = self.id {
			let attributedTitle = NSAttributedString(string: id, attributes: attributes)
			
			self.attributedTitle = attributedTitle
		}
		
	}
}
