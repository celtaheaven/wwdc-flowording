import Foundation

public struct SetupOptions {
	public let explorationDepth: Int
	public let maximumExperienceTime: Int
	
	public init(explorationDepth: Int = 5, maximumExperienceTime: Int = 180) {
		self.explorationDepth = explorationDepth
		self.maximumExperienceTime = maximumExperienceTime
	}
}
