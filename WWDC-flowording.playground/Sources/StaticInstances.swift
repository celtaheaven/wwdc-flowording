import UIKit
import Foundation
import CoreGraphics

public struct ExplorationDepth {
	public static let min = 3
	public static let max = 10
}

public struct MaximumExperienceTime {
	public static let min = 60
	public static let noLimit = 0
}

public var defaultFont : UIFont {
	get {
		let font = UIFont(name: "HelveticaNeue", size: 18)
		return font ?? UIFont()
	}
}

public var boldFont : UIFont {
	get {
		let font = UIFont(name: "HelveticaNeue-Bold", size: 18)
		return font ?? UIFont()
	}
}

public let defaultImageURL = Bundle.main.url(forResource: "backgroundImage", withExtension: "png")

public let allFlows : [String: [String: Any?]] = ["flowording": ["ID": "flowording", "BaseString": "Welcome to my Scholarship Application for WWDC 2018. Today we are playing something very simple. In this text there are interactible words that will lead you to a discovery process. Tap whatever strong words that you feel related to. At the end of this, I'll try to determine your personality throughout your choices. Hope you enjoy it.",
																 "FontColor": UIColor.black,
																 "BackgroundColor": UIColor.white,
																 "FlowActs": [FlowAct(NSMakeRange(42, 4), segueID: "WWDC", attributes: [.bigfan:1]),
																			  FlowAct(NSMakeRange(120, 12) , segueID: "Interaction", attributes: [.light:1]),
																			  FlowAct(NSMakeRange(163, 9) , segueID: "Discovery", attributes: [.science:1]),
																			  FlowAct(NSMakeRange(195, 6) , segueID: "Virtue", attributes: [.light:1, .hand: 1]),
																			  FlowAct(NSMakeRange(241, 3) , segueID: "Death", attributes: [.light: -2]),
																			  FlowAct(NSMakeRange(318, 4) , segueID: "Hope", attributes: [.light: 2])]
	], "Interaction": ["ID": "Interaction", "BaseString": "A kind of action that occur as two or more objects have an effect upon one another. In physics, a fundamental interaction or fundamental force is a process by which elementary particles interact with each other. In sociology, social interaction is a dynamic, changing sequence of social actions between individuals (or groups) who modify their actions and reactions due to the actions by their interaction partner(s).",
					   "FontColor": UIColor.blue,
					   "BackgroundColor": UIColor.white,
					   "FlowActs": [FlowAct(NSMakeRange(10, 6), segueID: "Physics", attributes: [.melee: 2]),
									FlowAct(NSMakeRange(87, 7) , segueID: "Physics", attributes: [.science: 1]),
									FlowAct(NSMakeRange(148, 7) , segueID: "Process Philosophy", attributes: [.hand: -1]),
									FlowAct(NSMakeRange(215, 9) , segueID: "Sociology", attributes: [.hand: -2]),
									FlowAct(NSMakeRange(280, 14) , segueID: "Sociology", attributes: [ .hand:-1,  .moral:1]),
									FlowAct(NSMakeRange(344, 7) , segueID: "Physics", attributes: [.melee:2]),
									FlowAct(NSMakeRange(356, 9) , segueID: "Physics", attributes: [.melee:3])]
	], "Discovery": ["ID": "Discovery", "BaseString": "Is the act of detecting something new, or something 'old' that had been unrecognized as meaningful. May sometimes be based on earlier discoveries, collaborations, or ideas. Some discoveries represent a radical breakthrough in knowledge or technology.",
					 "FontColor": UIColor.green,
					 "BackgroundColor": UIColor.brown,
					 "FlowActs": [FlowAct(NSMakeRange(34, 3), segueID: "Time", attributes: [ .hand:-2]),
								  FlowAct(NSMakeRange(52, 5) , segueID: "Time", attributes: [ .hand:2]),
								  FlowAct(NSMakeRange(88, 10) , segueID: "Reasoning", attributes: [ .moral:1]),
								  FlowAct(NSMakeRange(166, 5) , segueID: "Ideas", attributes: [.science:1]),
								  FlowAct(NSMakeRange(226, 9) , segueID: "Knowledge", attributes: [.science:2,  .hand:-1]),
								  FlowAct(NSMakeRange(239, 10) , segueID: "Technology", attributes: [.science:2,  .hand:1])]
	], "Virtue": ["ID": "Virtue", "BaseString": "Is moral excellence; a trait or quality that is deemed to be morally good and thus is valued as a foundation of principle and good moral being. Personal virtues are characteristics valued as promoting collective and individual greatness. The ancient Romans used the Latin word “virtus” to refer to all of the “excellent qualities of men, including physical strength, valorous conduct, and moral rectitude”.",
				  "FontColor": UIColor.cyan,
				  "BackgroundColor": UIColor.black,
				  "FlowActs": [FlowAct(NSMakeRange(201, 10), segueID: "Sociology", attributes: [.light:1]),
							   FlowAct(NSMakeRange(216, 10) , segueID: "Sociology", attributes: [.light:-2])]
	], "Death": ["ID": "Death", "BaseString": "The cessation of all biological functions that sustain a living organism. Particularly between the humans, it has been considered a sad or unpleasant occasion, due to the affection for the being that has died and the termination of social and familial bounds with the deceased. In most religions, it is just part of a process, being part of or what leads to eternity. ​Very problematic to be defined due to the many approaches in forms of knowledge.",
				 "FontColor": UIColor.white,
				 "BackgroundColor": UIColor.black,
				 "FlowActs": [FlowAct(NSMakeRange(21, 10), segueID: "Life", attributes: [.science:1]),
							  FlowAct(NSMakeRange(99, 6) , segueID: "Humanity", attributes: [ .hand:-1]),
							  FlowAct(NSMakeRange(243, 15) , segueID: "Humanity", attributes: [ .hand:2, .light:1]),
							  FlowAct(NSMakeRange(286, 9) , segueID: "Belief", attributes: [.melee:-1,  .hand:2]),
							  FlowAct(NSMakeRange(439, 9) , segueID: "Knowledge", attributes: [.science:2,  .hand:-1])]
	], "Hope": ["ID": "Hope", "BaseString": "Optimist state of mind that is based on an expectation of positive outcomes with respect to events and circumstances in one's life or the world at large. Specialists argues that hope comes into its own when crisis looms, opening us to new creative possibilities, but also with a determined plan it links to the existence of a goal.",
				"FontColor": UIColor.black,
				"BackgroundColor": UIColor.clear,
				"FlowActs": [FlowAct(NSMakeRange(9, 22-9), segueID: "Emotion", attributes: [.melee:-2]),
							 FlowAct(NSMakeRange(239, 8) , segueID: "Art", attributes: [.science:-3]),
							 FlowAct(NSMakeRange(326, 4) , segueID: "WWDC", attributes: [.melee:1, .unknown:1])]
	], "WWDC": ["ID": "WWDC", "BaseString": "The Apple Worldwide Developers Conference (WWDC) is a conference held annually by Apple Inc. The event is used to showcase new software and technologies for software developers. Attendees can participate in hands-on labs with Apple engineers, and in-depth sessions covering a wide variety of topics.",
				"FontColor": UIColor(displayP3Red: 0.3, green: 0.3, blue: 0.3, alpha: 1),
				"BackgroundColor": UIColor.white,
				"FlowActs": [FlowAct(NSMakeRange(4, 5), segueID: "Apple", attributes: [.bigfan:1, .unknown: 1]),
							 FlowAct(NSMakeRange(43, 4) , segueID: "Paradox", attributes: [.bigfan:2, .unknown: 2]),
							 FlowAct(NSMakeRange(82, 9) , segueID: "Apple Inc", attributes: [.bigfan:2]),
							 FlowAct(NSMakeRange(127, 8) , segueID: "Technology", attributes: [.science:1]),
							 FlowAct(NSMakeRange(140, 12) , segueID: "Technology", attributes: [.science:1,  .moral:1]),
							 FlowAct(NSMakeRange(157, 176-157) , segueID: "Working Class", attributes: [.light: 1,.science:1]),
							 FlowAct(NSMakeRange(226, 5) , segueID: "Apple", attributes: [.bigfan:1, .unknown:1]),
							 FlowAct(NSMakeRange(276, 4) , segueID: "Universe", attributes: [:])]
	], "Apple": ["ID": "Apple", "BaseString": "A sweet, edible fruit produced by a tree. Those trees are cultivated worldwide and are originated in Central Asia. The production of apples in 2014 was 84.6 million tonnes, with China accounting for 48% of the total. Often appear as religious symbols for eternal youthfulness, forbiddance and knowledge.",
				 "FontColor": UIColor(displayP3Red: 0.80, green: 0.76, blue: 0.5, alpha: 1),
				 "BackgroundColor": UIColor(displayP3Red: 0.6, green: 0.2, blue: 0.2, alpha: 1),
				 "FlowActs": [FlowAct(NSMakeRange(69, 9), segueID: "World", attributes: [.light:1]),
							  FlowAct(NSMakeRange(233, 9) , segueID: "Belief", attributes: [.melee:-1,  .hand:2]),
							  FlowAct(NSMakeRange(293, 9) , segueID: "Knowledge", attributes: [ .hand:-1])]
	], "Apple Inc": ["ID": "Apple Inc", "BaseString": "A multinational technology company that designs, develops, and sells consumer electronics, computer software, and online services. Founded by some amazing guys in April 1976 to develop and sell Wozniak's Apple I personal computer. Over the years, shipped new technologies featuring innovative graphical user interfaces, and it's marketing commercials for its products received widespread critical acclaim.",
					 "FontColor": UIColor(displayP3Red: 0.3, green: 0.3, blue: 0.3, alpha: 1),
					 "BackgroundColor": UIColor.clear,
					 "FlowActs": [FlowAct(NSMakeRange(40, 7), segueID: "Art", attributes: [.melee:-2]),
								  FlowAct(NSMakeRange(49, 8) , segueID: "Working Class", attributes: [.melee:1]),
								  FlowAct(NSMakeRange(63, 5) , segueID: "Capitalism", attributes: [ .hand:2]),
								  FlowAct(NSMakeRange(259, 12) , segueID: "Technology", attributes: [.science:1]),
								  FlowAct(NSMakeRange(282, 10) , segueID: "Ideas", attributes: [.science:1, .melee:-1])]
	], "Physics": ["ID": "Physics", "BaseString": "Known as 'knowledge of nature', is the natural science that studies matter and its motion and behavior through space and time and that studies the related entities of energy and force. These theoretical breakthroughs also make significant contributions by enabling advances in new technologies.",
				   "FontColor": UIColor(displayP3Red: 0.8, green: 0.8, blue: 0.8, alpha: 1),
				   "BackgroundColor": UIColor(displayP3Red: 0.2, green: 0.25, blue: 0.2, alpha: 1),
				   "FlowActs": [FlowAct(NSMakeRange(10, 9), segueID: "Knowledge", attributes: [.science:1]),
								FlowAct(NSMakeRange(110, 15) , segueID: "Universe", attributes: [.science:1, .melee:-1]),
								FlowAct(NSMakeRange(281, 12) , segueID: "Technology", attributes: [.science:1,  .hand:1])]
	], "Process Philosophy": ["ID": "Process Philosophy", "BaseString": "Also known as ontology, identifies metaphysical reality with change and development. Since the old times, philosophers have posited true reality as 'timeless', based on permanent substances, while processes are denied or subordinated to timeless substances. Therefore, classic ontology denies any full reality to change, which is conceived as only accidental and not essential. Later in opposition to the classical model, process philosophy regards change as the cornerstone of reality.",
							  "FontColor": UIColor(displayP3Red: 0.2, green: 0.2, blue: 0.2, alpha: 1),
							  "BackgroundColor": UIColor.white,
							  "FlowActs": [FlowAct(NSMakeRange(149, 8), segueID: "Nihilism", attributes: [.light:-1]),
										   FlowAct(NSMakeRange(221, 12) , segueID: "Totalitarianism", attributes: [.light:-2]),
										   FlowAct(NSMakeRange(363, 13) , segueID: "Reasoning", attributes: [.melee:1]),
										   FlowAct(NSMakeRange(441, 14) , segueID: "Reasoning", attributes: [ .hand:-1])]
	], "Sociology": ["ID": "Sociology", "BaseString": "Scientific study of society, including patterns of social relationships, social interaction, and culture. It is a social science that uses various methods of empirical investigation and critical analysis to develop a body of knowledge about social order, acceptance, and change. Subject matter ranges from the micro-sociology level of individual agency and interaction to the macro level of systems and the social structure.",
					 "FontColor": UIColor(displayP3Red: 0.2, green: 0.2, blue: 0.2, alpha: 1),
					 "BackgroundColor": UIColor.white,
					 "FlowActs": [FlowAct(NSMakeRange(225, 9), segueID: "Knowledge", attributes: [.melee:-1, .science:1]),
								  FlowAct(NSMakeRange(241, 12) , segueID: "Communism", attributes: [.hand:-1]),
								  FlowAct(NSMakeRange(255, 10) , segueID: "Totalitarianism", attributes: [.light:-1]),
								  FlowAct(NSMakeRange(271, 6) , segueID: "Capitalism", attributes: [.hand:1, .melee:1])]
	], "Time": ["ID": "Time", "BaseString": "Indefinite continued progress of existence and events that occur in apparently irreversible succession from the past through the present to the future. A component quantity of various measurements used to sequence events, to compare the duration of events or the intervals between them, and to quantify rates of change of quantities in material reality or in the conscious experience. Is often referred to as a fourth dimension, along with three spatial dimensions.",
				"FontColor": UIColor.black,
				"BackgroundColor": UIColor(displayP3Red: 0.91, green: 0.82, blue: 0.45, alpha: 1),
				"FlowActs": [FlowAct(NSMakeRange(112, 4), segueID: "Belief", attributes: [.melee:-1]),
							 FlowAct(NSMakeRange(129, 7) , segueID: "Knowledge", attributes: [.melee:1]),
							 FlowAct(NSMakeRange(144, 6) , segueID: "Death", attributes: [.melee:2]),
							 FlowAct(NSMakeRange(312, 6) , segueID: "Evolution", attributes: [ .hand:1]),
							 FlowAct(NSMakeRange(411, 53) , segueID: "Universe", attributes: [.melee:-3,.science:1])]
	], "Reasoning": ["ID": "Reasoning", "BaseString": "Associated with thinking, cognition, and intellect. The philosophical field of logic studies ways in which humans reason formally through argument. Like habit or intuition, is one of the ways by which thinking comes from one idea to a related idea. It is widely discussed if whether animals other than humans can reason.",
					 "FontColor": UIColor.blue,
					 "BackgroundColor": UIColor(displayP3Red: 0.85, green: 0.65, blue: 0.25, alpha: 1),
					 "FlowActs": [FlowAct(NSMakeRange(162, 9), segueID: "Belief", attributes: [.science:-1]),
								  FlowAct(NSMakeRange(283, 25) , segueID: "Life", attributes: [.light:1, .melee:-1]),
								  FlowAct(NSMakeRange(313, 6) , segueID: "Belief", attributes: [ .moral:-1])]
	], "Ideas": ["ID": "Ideas", "BaseString": "In philosophy, they are usually construed as mental representation images of some object. Ideas can also be abstract concepts that do not present as mental images. The capacity to create and understand the meaning of ideas is considered to be an essential and defining feature of human beings. In a popular sense, an idea arises in a reflexive, spontaneous manner, even without thinking or serious reflection, for example, when we talk about the idea of a person or a place.",
				 "FontColor": UIColor.cyan,
				 "BackgroundColor": UIColor.blue,
				 "FlowActs": [FlowAct(NSMakeRange(246, 46), segueID: "Humanity", attributes: [ .moral:2]),
							  FlowAct(NSMakeRange(299, 14) , segueID: "Reasoning", attributes: [ .hand:-1]),
							  FlowAct(NSMakeRange(431, 4) , segueID: "Interaction", attributes: [.melee:1])]
	], "Knowledge": ["ID": "Knowledge", "BaseString": "A familiarity, awareness, or understanding of someone or something, such as facts, information, descriptions, or skills, which is acquired through experience or education by perceiving, discovering, or learning. Can refer to a theoretical or practical understanding of a subject; either it be implicit or explicit, more or less formal or systematic.",
					 "FontColor": UIColor.black,
					 "BackgroundColor": UIColor.white,
					 "FlowActs": [FlowAct(NSMakeRange(46, 7), segueID: "Life", attributes: [ .moral:1,.science:-1]),
								  FlowAct(NSMakeRange(57, 9) , segueID: "Objecthood", attributes: [ .moral:-1,.science:1]),
								  FlowAct(NSMakeRange(147, 10) , segueID: "Emotion", attributes: [.melee:2])]
	], "Technology": ["ID": "Technology", "BaseString": "Known as 'the science and art of craft', is the collection of techniques, skills, methods, and processes used in the production of goods or services or in the accomplishment of objectives, such as scientific investigation. It is present in the simple form of controlling fire to the most advanced machines.",
					  "FontColor": UIColor(displayP3Red: 0.8, green: 0.95, blue: 0.95, alpha: 1),
					  "BackgroundColor": UIColor(displayP3Red: 0.2, green: 0.3, blue: 0.55, alpha: 1),
					  "FlowActs": [FlowAct(NSMakeRange(14, 7), segueID: "Knowledge", attributes: [ .hand:1,.science:1]),
								   FlowAct(NSMakeRange(26, 3), segueID: "Art", attributes: [ .hand:-1,.science:1]),
								   FlowAct(NSMakeRange(33, 5) , segueID: "Working Class", attributes: [.melee:1]),
								   FlowAct(NSMakeRange(229, 7) , segueID: "Time", attributes: [.melee:-1])]
	], "Life": ["ID": "Life", "BaseString": "Characteristic that distinguishes physical entities that do have biological process, such as signalling and self-sustaining processes, from those that do not, either because such functions have ceased, or because they never had such functions and are classified as inanimate. In Biology, the definition of life is controversial due to the existence of viruses and viroids, which are recognized forms of life that are non-cellular, different from us and other celular based beings.",
				"FontColor": UIColor(displayP3Red: 0.95, green: 0.95, blue: 0.95, alpha: 1),
				"BackgroundColor": UIColor(displayP3Red: 0.37, green: 0.37, blue: 0.19, alpha: 1),
				"FlowActs": [FlowAct(NSMakeRange(34, 8), segueID: "Physics", attributes: [.melee:1]),
							 FlowAct(NSMakeRange(140, 17), segueID: "Objecthood", attributes: [ .moral:-2]),
							 FlowAct(NSMakeRange(314, 13), segueID: "Paradox", attributes: [ .moral:-1,  .hand:-1])]
	], "Humanity": ["ID": "Humanity", "BaseString": "A Virtue associated with basic ethics of altruism derived from the human condition. Differs from mere justice in that there is a level of altruism towards individuals included in humanity more so than the fairness found in justice. That is, humanity, and the acts of love, altruism, and social intelligence are typically individual strengths while fairness is generally expanded to all.",
					"FontColor": UIColor(displayP3Red: 0.37, green: 0.37, blue: 0.19, alpha: 1),
					"BackgroundColor": UIColor(displayP3Red: 0.95, green: 0.95, blue: 0.95, alpha: 1),
					"FlowActs": [FlowAct(NSMakeRange(2, 6), segueID: "Virtue", attributes: [ .moral:1]),
								 FlowAct(NSMakeRange(241, 40), segueID: "Emotion", attributes: [ .moral:1,.light:1]),
								 FlowAct(NSMakeRange(348, 8), segueID: "Democracy", attributes: [ .moral:1, .light:2])]
	], "Belief": ["ID": "Belief", "BaseString": "State of mind in which a person thinks something to be the case with or without there being empirical evidence to prove that something is the case with factual certainty. In religion, there are different forms of belief, such as Fundamentalism, Orthodoxy, Modern, Superstition, Systematization and Universalism; and there are approaches to other ones beliefs, such as exclusivism, inclusivism, pluralism, syncretism.",
				  "FontColor": UIColor.black,
				  "BackgroundColor": UIColor(displayP3Red: 0.95, green: 0.95, blue: 0.75, alpha: 1),
				  "FlowActs": [FlowAct(NSMakeRange(0, 13), segueID: "Emotion", attributes: [.melee:-1]),
							   FlowAct(NSMakeRange(39, 9) , segueID: "Objecthood", attributes: [.light:-1]),
							   FlowAct(NSMakeRange(152, 17) , segueID: "Emotion", attributes: [.science:-1]),
							   FlowAct(NSMakeRange(298, 7) , segueID: "Universe", attributes: [ .moral:1]),
							   FlowAct(NSMakeRange(368, 11) , segueID: "Totalitarianism", attributes: [.light:-3]),
							   FlowAct(NSMakeRange(381, 11) , segueID: "Democracy", attributes: [.light:1,  .hand:2,  .moral:1]),
							   FlowAct(NSMakeRange(394, 9) , segueID: "Communism", attributes: [.light:1,  .hand:-2,  .moral:1])]
	], "Emotion": ["ID": "Emotion", "BaseString": "Is any conscious experience characterized by intense mental activity and a certain degree of pleasure or displeasure. Scientific discourse has drifted to other meanings and there is no consensus on a definition. In some theories, cognition is an important aspect of emotion. Emotions are complex. Are you happy or sad? Or rather amazed or confused? It may seem that they are antagonists to themselves, but remember, the true antagonism of emotion is to feel absent of it.",
				   "FontColor": UIColor(displayP3Red: 0.7, green: 0.2, blue: 0.2, alpha: 1),
				   "BackgroundColor": UIColor.white,
				   "FlowActs": [FlowAct(NSMakeRange(17, 10), segueID: "Art", attributes: [.melee:2]),
								FlowAct(NSMakeRange(93, 8), segueID: "Interaction", attributes: [.melee:3,  .moral:-1]),
								FlowAct(NSMakeRange(105, 11), segueID: "Interaction", attributes: [.melee:-1,  .moral:1])]
	], "Nihilism": ["ID": "Nihilism", "BaseString": "Philosophical viewpoint that suggests the denial or lack of belief towards the reputedly meaningful aspects of life. Most commonly is presented in the form of existential nothingness, which argues that life is without objective meaning, purpose, or… Sorry, I'm stopping because this is dangerous.",
					"FontColor": UIColor.gray,
					"BackgroundColor": UIColor.white,
					"FlowActs": [FlowAct(NSMakeRange(60, 6), segueID: "Belief", attributes: [.moral:1]),
								 FlowAct(NSMakeRange(89, 11), segueID: "Reasoning", attributes: [.science:1])]
	], "Art": ["ID": "Art", "BaseString": "Is a diverse range of human activities in creating visual, auditory or performing artifacts, expressing the author's imaginitive or technical skill, intended to be appreciated for their beauty or emotional power. Music, theatre, film, dance, and other performing arts, as well as literature and other media such as interactive media, are included in a broader definition of art or the arts. Only beware of relativism, it only serves to discourage technical work  on the basis of less complex proposals, relativizing that any artifact can be seen as art.",
			   "FontColor": UIColor(displayP3Red: 1, green: 0.39, blue: 0.56, alpha: 1),
			   "BackgroundColor": UIColor(displayP3Red: 0.48, green: 0.2, blue: 0.48, alpha: 1),
			   "FlowActs": [FlowAct(NSMakeRange(396, 6), segueID: "Reasoning", attributes: [.science:1]),
							FlowAct(NSMakeRange(406, 10), segueID: "Nihilism", attributes: [.hand:-2,.light:1]),
							FlowAct(NSMakeRange(436, 25) , segueID: "Communism", attributes: [.hand:-3,.science:-1]),
							FlowAct(NSMakeRange(479, 22) , segueID: "Illusion", attributes: [ .hand:-1,  .moral:-2,.science:-1])]
	], "Universe": ["ID": "Universe", "BaseString": "Is all of space and time and their contents, including planets, stars, galaxies, and all other forms of matter and energy. While the size of the entire Universe is still unknown, it is possible to measure the observable universe. The Universe is often defined as 'the totality of existence', or everything that exists, everything that has existed, and everything that will exist.",
					"FontColor": UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.8),
					"BackgroundColor": UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.8),
					"FlowActs": [FlowAct(NSMakeRange(10, 5), segueID: "World", attributes: [.science:1, .melee:1]),
								 FlowAct(NSMakeRange(20, 4), segueID: "Time", attributes: [.science:-1, .melee:-1]),
								 FlowAct(NSMakeRange(264, 25), segueID: "The Totality of Existence", attributes: [.science:-1])]
	], "Working Class": ["ID": "Working Class", "BaseString": "Also known as the proletariat, are the people employed for wages, especially in manual-labour occupations and industrial work. The most general definition, used by Marxists and socialists, is that the working class includes all those who have nothing to sell but their labor-power and skills; and if the working class produces everything, then everything should belong to them.",
						 "FontColor": UIColor.black,
						 "BackgroundColor": UIColor.white,
						 "FlowActs": [FlowAct(NSMakeRange(18, 11), segueID: "Paradox", attributes: [ .moral:-1]),
									  FlowAct(NSMakeRange(164, 8), segueID: "Communism", attributes: [ .hand:-1]),
									  FlowAct(NSMakeRange(339, 37), segueID: "World", attributes: [.light:1])]
	], "World": ["ID": "World", "BaseString": "Is the planet Earth and all life upon it, including human civilization. In a philosophical context, the 'world' is the whole of the physical Universe, or an ontological world (the 'world' of an individual). In a theological context, the world is the material or the profane sphere, as opposed to the celestial, spiritual, transcendent or sacred. The 'end of the world' refers to scenarios of the final end of human history, often in religious contexts.",
				 "FontColor": UIColor(displayP3Red: 0.45, green: 1, blue: 0.47, alpha: 0.8),
				 "BackgroundColor": UIColor(displayP3Red: 0.25, green: 0.33, blue: 0.81, alpha: 1),
				 "FlowActs": [FlowAct(NSMakeRange(24, 8), segueID: "Life", attributes: [ .moral:-1]),
							  FlowAct(NSMakeRange(52, 18) , segueID: "Humanity", attributes: [.light:1,  .moral:1]),
							  FlowAct(NSMakeRange(157, 11) , segueID: "Process Philosophy", attributes: [.science:-1, .melee:-1]),
							  FlowAct(NSMakeRange(351, 16) , segueID: "Death", attributes: [.light:-2]),
							  FlowAct(NSMakeRange(433, 9) , segueID: "Belief", attributes: [.melee:-1,.science:-1])]
	], "Capitalism": ["ID": "Capitalism", "BaseString": "Economic system based upon private ownership of the means of production and their operation for profit. Critics of capitalism argue that it establishes power in the hands of a minority capitalist class that exists through the exploitation of the majority working class; prioritizes profit over social good, natural resources and the environment; and is an engine of inequality and economic instabilities.",
					  "FontColor": UIColor.white,
					  "BackgroundColor": UIColor(displayP3Red: 190/255, green: 68/255, blue: 50/255, alpha: 1),
					  "FlowActs": [FlowAct(NSMakeRange(27, 17), segueID: "Paradox", attributes: [ .hand:1]),
								   FlowAct(NSMakeRange(52, 19), segueID: "Working Class", attributes: [.light:1]),
								   FlowAct(NSMakeRange(104, 21) , segueID: "Communism", attributes: [ .hand:-1]),
								   FlowAct(NSMakeRange(226, 12) , segueID: "Totalitarianism", attributes: [.light:-1])]
	], "Communism": ["ID": "Communism", "BaseString": "In political and social sciences, is the philosophical, social, political and economic ideology and movement whose ultimate goal is the establishment of the communist society, which is a socioeconomic order structured upon the common ownership of the means of production and the absence of social classes, money and the state. Critics of socialism mainly argue that it doesn’t works, humanity will never be mature enough to such hypothetical progressive polity.",
					 "FontColor": UIColor.yellow,
					 "BackgroundColor": UIColor.red,
					 "FlowActs": [FlowAct(NSMakeRange(17, 15), segueID: "Sociology", attributes: [ .hand:-1,.science:-1]),
								  FlowAct(NSMakeRange(115, 13), segueID: "Ultimate Goal", attributes: [.melee:1]),
								  FlowAct(NSMakeRange(251, 19) , segueID: "Working Class", attributes: [.light:1]),
								  FlowAct(NSMakeRange(327, 20) , segueID: "Capitalism", attributes: [ .hand:1])]
	], "Totalitarianism": ["ID": "Totalitarianism", "BaseString": "A political concept where the state recognizes no limits to its authority and strives to regulate every aspect of public and private life wherever feasible. Totalitarian regimes stay in political power through rule by one leader and an all-encompassing propaganda campaign, which is disseminated through the state-controlled mass media, a single party that is often marked by political repression, personality cultism, control over the economy, regulation and restriction of speech, mass surveillance and widespread use of terror. Are you in favor or against it?",
						   "FontColor": UIColor.black,
						   "BackgroundColor": UIColor(displayP3Red: 202/255, green: 43/255, blue: 29/255, alpha: 1),
						   "FlowActs": [FlowAct(NSMakeRange(542, 5), segueID: "Death", attributes: [.light:-2]),
										FlowAct(NSMakeRange(551, 7), segueID: "Democracy", attributes: [.light:-2])]
	], "Democracy": ["ID": "Democracy", "BaseString": "Is a system of government in which the citizens exercise power directly or elect representatives from among themselves to form a governing body, such as a parliament. Is sometimes referred to as 'rule of the majority'. Critics of democracy mainly argue that it is totally inefficient, leads to non-effective acts, has fraudulent elections and the popular rule is a facade.",
					 "FontColor": UIColor.white,
					 "BackgroundColor": UIColor(displayP3Red: 202/255, green: 43/255, blue: 29/255, alpha: 1),
					 "FlowActs": [FlowAct(NSMakeRange(219, 20), segueID: "Totalitarianism", attributes: [.light:-1]),
								  FlowAct(NSMakeRange(294, 18), segueID: "Operation for Profit", attributes: [.light:1])]
	], "Paradox": ["ID": "Paradox", "BaseString": "A statement that, despite apparently sound reasoning from true premises, leads to an apparently self-contradictory or logically unacceptable conclusion. A paradox involves contradictory yet interrelated elements that exist simultaneously and persist over time. Some logical paradoxes are known to be invalid arguments and have revealed errors in definitions assumed to be rigorous, but are still valuable in promoting critical thinking.",
				   "FontColor": UIColor.white,
				   "BackgroundColor": UIColor(displayP3Red: 0.3, green: 0.3, blue: 0.3, alpha: 1),
				   "FlowActs": [FlowAct(NSMakeRange(37, 15), segueID: "Belief", attributes: [ .moral:1, .melee:-1]),
								FlowAct(NSMakeRange(118, 33), segueID: "Knowledge", attributes: [.science:1])]
	], "Evolution": ["ID": "Evolution", "BaseString": "The change in the heritable characteristics of biological populations over successive generations. Evolutionary processes give rise to biodiversity at every level of biological organisation, including the levels of species, individual organisms, and molecules.",
					 "FontColor": UIColor(displayP3Red: 0.2, green: 0.21, blue: 0.34, alpha: 1),
					 "BackgroundColor": UIColor(displayP3Red: 0.35, green: 0.64, blue: 0.84, alpha: 1),
					 "FlowActs": [FlowAct(NSMakeRange(47, 10), segueID: "Life", attributes: [.science:1]),
								  FlowAct(NSMakeRange(75, 22), segueID: "Paradox", attributes: [.light:1]),
								  FlowAct(NSMakeRange(166, 23), segueID: "Sociology", attributes: [ .moral:1])]
	], "Objecthood": ["ID": "Objecthood", "BaseString": "Is the state of being an object. One approach to defining it is in terms of objects' properties and relations. Descriptions of all bodies, minds, and persons must be in terms of their properties and relations. The philosophical question of the nature of objecthood concerns how objects are related to their properties and relations.",
					 "FontColor": UIColor.black,
					 "BackgroundColor": UIColor.white,
					 "FlowActs": [FlowAct(NSMakeRange(127, 30), segueID: "Universe", attributes: [.science:1,  .moral:-1]),
								  FlowAct(NSMakeRange(199, 9), segueID: "Interaction", attributes: [.light:1]),
								  FlowAct(NSMakeRange(290, 27), segueID: "The Totality of Existence", attributes: [.melee:1, .light:-1])]
	], "Illusion": ["ID": "Illusion", "BaseString": "A distortion of the senses, revealing how the animal brain normally organizes and interprets sensory stimulation. Though illusions distort reality, they are generally shared by most people. Illusions may occur with any of the human senses, but visual illusions are the best-known and understood. The emphasis on visual illusions occurs because vision often dominates the other senses.",
					"FontColor": UIColor.white,
					"BackgroundColor": UIColor(displayP3Red: 0.3, green: 0.3, blue: 0.3, alpha: 1),
					"FlowActs": [FlowAct(NSMakeRange(82, 30), segueID: "Discovery", attributes: [.melee:1]),
								 FlowAct(NSMakeRange(357, 26), segueID: "Totalitarianism", attributes: [.light:-1])]
	], "Extraterrestrial Life": ["ID": "Extraterrestrial Life", "BaseString": "Also called alien life, is life that occurs outside of Earth and that probably did not originate from Earth. There are expectation about the existence of intelligent life elsewhere in the universe. The science of extraterrestrial life in all its forms is known as exobiology. '連邦政府軍のご協力により、君達の基地は、全てCATSがいただいた。'",
								 "FontColor": UIColor(displayP3Red: 0.3, green: 1, blue: 0.3, alpha: 1),
								 "BackgroundColor": UIColor(displayP3Red: 0.3, green: 0.3, blue: 0.3, alpha: 1),
								 "FlowActs": [FlowAct(NSMakeRange(55, 5), segueID: "World", attributes: [.melee:1]),
											  FlowAct(NSMakeRange(83, 24), segueID: "Paradox", attributes: [.melee:-1]),
											  FlowAct(NSMakeRange(154, 16) , segueID: "Life", attributes: [.science:-1]),
											  FlowAct(NSMakeRange(202, 32) , segueID: "Knowledge", attributes: [.science:1]),
											  FlowAct(NSMakeRange(277, 33) , segueID: "Totalitarianism", attributes: [ .moral:-2])]
	], "The Totality of Existence": ["ID": "The Totality of Existence", "BaseString": "How are you feeling today? Amazing? Tired? Rather other emotion? There are many on the same boat as you. Some are swimming.",
									 "FontColor": UIColor(displayP3Red: 0, green: 0.8, blue: 0.8, alpha: 1),
									 "BackgroundColor": UIColor(displayP3Red: 0.3, green: 0.3, blue: 0.3, alpha: 1),
									 "FlowActs": [FlowAct(NSMakeRange(27, 7), segueID: "Emotion", attributes: [.melee:2]),
												  FlowAct(NSMakeRange(36, 5), segueID: "Emotion", attributes: [.melee:-2]),
												  FlowAct(NSMakeRange(87, 9) , segueID: "Reasoning", attributes: [.light:2, .unknown:2]),
												  FlowAct(NSMakeRange(114, 8) , segueID: "Working Class", attributes: [ .moral:-1, .unknown:2])]
	], "Ultimate Goal": ["ID": "Ultimate Goal", "BaseString": "Do you believe that any animal will reach the status of natural goodness? No or... we are not animals?",
						 "FontColor": UIColor(displayP3Red: 1, green: 1, blue: 0.9, alpha: 1),
						 "BackgroundColor": UIColor.gray,
						 "FlowActs": [FlowAct(NSMakeRange(74, 2), segueID: "Reasoning", attributes: [.unknown:2,.science:1]),
									  FlowAct(NSMakeRange(81, 18), segueID: "Illusion", attributes: [.unknown:2,.science:-2])]
	], "Operation for Profit": ["ID": "Operation for Profit", "BaseString": "Do you believe that humanity will ever be able to feel definitive accomplishment? Yes or... the money is the only solution to it.",
								"FontColor": UIColor.white,
								"BackgroundColor": UIColor(displayP3Red: 190/255, green: 68/255, blue: 50/255, alpha: 1),
								"FlowActs": [FlowAct(NSMakeRange(82, 3), segueID: "Belief", attributes: [.unknown:2,  .hand:-2]),
											 FlowAct(NSMakeRange(90, 36), segueID: "Capitalism", attributes: [.unknown:2,  .hand:2])]
	]]

public let errorFlow : [String: Any?] = ["ID": "Errorrrrrrr", "BaseString": "Hey homie, please, configure the SetupOptions struct according the instructions. Or else we keep standing here until the end of the universe... or until this device's battery turns dry.",
						"FontColor": UIColor.white,
						"BackgroundColor": UIColor.black
]
