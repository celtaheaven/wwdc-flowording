import UIKit
import Foundation
import CoreImage
import CoreAudio

public class FlowViewController : UIViewController {
	
	var session = Session()
	var options = SetupOptions()
	
	lazy var backgroundView: UIImageView = {
		let instance = UIImageView(image: UIImage(contentsOfFile: defaultImageURL!.relativePath))
		instance.backgroundColor = UIColor.white
		instance.isUserInteractionEnabled = true
		return instance
	}()
	
	lazy var titleLabel: UILabel = {
		let instance = UILabel()
		instance.text = ""
		instance.textAlignment = .center
		instance.isUserInteractionEnabled = true
		return instance
	}()
	
	lazy var countdownLabel: UILabel = {
		let instance = UILabel()
		instance.font = defaultFont
		instance.text = "0:00"
		return instance
	}()
	
	lazy var textView: UITextView = {
		let instance = UITextView()
		instance.isEditable = false
		instance.isSelectable = false
		instance.backgroundColor = UIColor.clear
		instance.isUserInteractionEnabled = true
		instance.addGestureRecognizer(tapGER)
		return instance
	}()
	
	lazy var tapGER: UITapGestureRecognizer = {
		let instance = UITapGestureRecognizer(target: self, action: #selector(tap(sender:)))
		instance.numberOfTouchesRequired = 1
		instance.numberOfTapsRequired = 1
		return instance
	}()
	
	var timer: Timer?
	
	public convenience init(options: SetupOptions) {
		self.init()
		self.options = options
	}
	
	public override func loadView() {
		let view = UIView()
		self.view = view
		
		self.setupViewHierarchy()
		self.setupConstraints()
		self.setupNewSession()
	}
	
	func setupViewHierarchy(){
		self.view.addSubview(backgroundView)
		self.backgroundView.addSubview(titleLabel)
		self.backgroundView.addSubview(countdownLabel)
		self.backgroundView.addSubview(textView)
	}
	
	func setupConstraints(){
		// SETTING UP VIEWS
		// -- BACKGROUND VIEW
		backgroundView.translatesAutoresizingMaskIntoConstraints = false
		backgroundView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
		backgroundView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
		backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
		backgroundView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
		
		// -- TITLE LABEL
		titleLabel.sizeToFit()
		titleLabel.translatesAutoresizingMaskIntoConstraints = false
		titleLabel.topAnchor.constraint(equalTo: backgroundView.topAnchor, constant: 20).isActive = true
		titleLabel.leftAnchor.constraint(equalTo: backgroundView.leftAnchor, constant: 20).isActive = true
		titleLabel.rightAnchor.constraint(equalTo: backgroundView.rightAnchor, constant: -20).isActive = true
		
		// -- COUNTDOWN LABEL
		countdownLabel.sizeToFit()
		countdownLabel.translatesAutoresizingMaskIntoConstraints = false
		countdownLabel.leftAnchor.constraint(equalTo: backgroundView.leftAnchor, constant: 20).isActive = true
		countdownLabel.rightAnchor.constraint(equalTo: backgroundView.rightAnchor, constant: -20).isActive = true
		countdownLabel.bottomAnchor.constraint(equalTo: backgroundView.bottomAnchor, constant: -20).isActive = true
		
		// -- MAIN TEXTVIEW
		textView.sizeToFit()
		textView.translatesAutoresizingMaskIntoConstraints = false
		textView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
		textView.bottomAnchor.constraint(equalTo: countdownLabel.topAnchor, constant: -20).isActive = true
		textView.leftAnchor.constraint(equalTo: backgroundView.leftAnchor, constant: 20).isActive = true
		textView.rightAnchor.constraint(equalTo: backgroundView.rightAnchor, constant: -20).isActive = true
	}
	
	func setupNewSession(){
		if ((options.maximumExperienceTime >= MaximumExperienceTime.min ||  options.maximumExperienceTime == MaximumExperienceTime.noLimit) && options.explorationDepth >= ExplorationDepth.min && options.explorationDepth <= ExplorationDepth.max){
			
			self.session = Session()
			self.session.currentFlow = Flow(params: allFlows["flowording"]!)
			if (options.maximumExperienceTime != MaximumExperienceTime.noLimit) {
				self.session.countdownTime = options.maximumExperienceTime
				self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
			} else {
				self.countdownLabel.alpha = 0
			}
			
		} else {
			self.session.currentFlow = Flow(params: errorFlow)
		}
		
		setupCurrentFlow()
	}
	
	@objc func tap (sender: UITapGestureRecognizer){
		if let textView = sender.view as? UITextView {
			let layoutManager = textView.layoutManager
			
			var location = sender.location(in: textView)
			location.x -= textView.textContainerInset.left
			location.y -= textView.textContainerInset.top
			
			let charIndex = layoutManager.characterIndex(for: location, in: textView.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
			
			if charIndex < textView.textStorage.length {
				let flowAttributes = textView.attributedText.attributes(at: charIndex, effectiveRange: nil)
				
				var segueValue : String = ""
				
				flowAttributes.forEach({ (key, value) in
					if key == NSAttributedStringKey(rawValue: "bite the dust"){
						self.setupNewSession()
						return
					}
					
					
					switch key {
					case NSAttributedStringKey(rawValue: "segueID"):
						segueValue = value as! String
					case NSAttributedStringKey(rawValue: FlowActStat.hand.rawValue):
						self.session.stats[.hand]! += value as! CGFloat
					case NSAttributedStringKey(rawValue: FlowActStat.moral.rawValue):
						self.session.stats[.moral]! += value as! CGFloat
					case NSAttributedStringKey(rawValue: FlowActStat.science.rawValue):
						self.session.stats[.science]! += value as! CGFloat
					case NSAttributedStringKey(rawValue: FlowActStat.light.rawValue):
						self.session.stats[.light]! += value as! CGFloat
					case NSAttributedStringKey(rawValue: FlowActStat.melee.rawValue):
						self.session.stats[.melee]! += value as! CGFloat
					case NSAttributedStringKey(rawValue: FlowActStat.bigfan.rawValue):
						self.session.stats[.bigfan]! += value as! CGFloat
					case NSAttributedStringKey(rawValue: FlowActStat.unknown.rawValue):
						self.session.stats[.unknown]! += value as! CGFloat
					default:
						break
					}
				})
				
				
				if (segueValue != ""){
					self.session.performedPath.append(self.session.currentFlow)
					
					if self.session.performedPath.count < options.explorationDepth {
						self.session.currentFlow = Flow(params: allFlows[segueValue]!)
						self.setupCurrentFlow()
					} else {
						self.endSession()
					}
				}
			}
			
		}
	}
	
	func setupCurrentFlow(){
		
		// ANIMATIONS
		let animation = CATransition()
		animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
		animation.type = kCATransitionFade
		animation.duration = 0.75
		
		self.textView.layer.add(animation, forKey: kCATransitionFade)
		self.titleLabel.layer.add(animation, forKey: kCATransitionFade)
		self.countdownLabel.layer.add(animation, forKey: kCATransitionFade)
		self.backgroundView.layer.add(animation, forKey: kCATransitionFade)
		
		let ciImage = CIImage(contentsOf:  defaultImageURL!)
		let colorFilter = CIFilter(name: "CIConstantColorGenerator")!
		colorFilter.setValue(self.session.currentFlow.backgroundColor?.coreImageColor, forKey: kCIInputColorKey)
		let colorImage = colorFilter.outputImage?.cropped(to: CGRect(x: 0, y: 0, width: self.backgroundView.frame.width, height: self.backgroundView.frame.height))
		let imageFilter = CIFilter(name: "CIMultiplyCompositing")!
		imageFilter.setValue(colorImage, forKey: kCIInputImageKey)
		imageFilter.setValue(ciImage, forKey: kCIInputBackgroundImageKey)
		let outputImage = imageFilter.outputImage?.cropped(to: CGRect(x: 0, y: 0, width: self.backgroundView.frame.width, height: self.backgroundView.frame.height))
		
		// SWITCHING VALUES
		self.backgroundView.image = outputImage?.customUIImage
		self.titleLabel.attributedText = self.session.currentFlow.attributedTitle
		self.textView.attributedText = self.session.currentFlow.attributedText
		
		self.textView.textColor = self.session.currentFlow.fontColor
		self.titleLabel.textColor = self.session.currentFlow.fontColor
		self.countdownLabel.textColor = self.session.currentFlow.fontColor
		
		self.textView.textAlignment = .justified
		
	}
	
	@objc func updateTimer(){
		if (self.session.countdownTime >= 0){
			let minutes = String(self.session.countdownTime/60)
			let seconds = String(format: "%02d",self.session.countdownTime%60)
			self.countdownLabel.text = "\(minutes):\(seconds)"
			self.session.countdownTime -= 1
		} else {
			self.endSession()
		}
	}
	
	func killTimer(){
		if self.timer != nil {
			self.timer!.invalidate()
			self.timer = nil
		}
	}
	
	func endSession(){
		self.killTimer()
		
		let endingTitle = "Thank you for your cooperation."
		var endingMessage = "It turns out you"
		
		if let hand =  self.session.stats[.hand] {
			switch hand {
			case 2...: endingMessage += "'re such a moneymaker,"
			case 1: endingMessage += " enjoys money,"
			case ...(-2): endingMessage += " cheers for the communism,"
			case -1: endingMessage += " dislikes money,"
			default: break
			}
		}
		
		if let moral = self.session.stats[.moral] {
			switch moral {
			case 2...: endingMessage += " embraces the Order, "
			case 1: endingMessage += " goes along with the Order, "
			case ...(-2): endingMessage += " embraces the Chaos, "
			case -1: endingMessage += " goes along with the Chaos, "
			default: endingMessage += " lives being neutral, "
			}
		}
		
		if let science = self.session.stats[.science] {
			switch science {
			case 2...: endingMessage += "longs for the numbers, "
			case 1: endingMessage += "can do some math, "
			case ...(-2): endingMessage += "lives with the nature, "
			case -1: endingMessage += "enjoys the nature, "
			default: break
			}
		}
		
		if let light = self.session.stats[.light] {
			switch light {
			case 2...: endingMessage += "puts people together, "
			case 1: endingMessage += "enjoys fellowship, "
			case ...(-2): endingMessage += "hates fellowship, "
			case -1: endingMessage += "enjoys yourself, "
			default: break
			}
		}
		
		if let melee = self.session.stats[.melee] {
			switch melee {
			case 2...: endingMessage += "and are a strong person."
			case 1: endingMessage += "and do stand up for activities."
			case ...(-2): endingMessage += "and are a philosopher."
			case -1: endingMessage += "and do stand up for thinking."
			default: endingMessage += "and thats it."
			}
		}
		
		if let bigfan = self.session.stats[.bigfan] {
			if bigfan >= 2 {
				endingMessage += " Also, you really "
				if bigfan >= 3 {
					endingMessage += "love apples. Strange."
				} else {
					endingMessage += "like apple."
				}
			}
		}
		
		if let uhh = self.session.stats[.unknown]{
			if uhh >= 3 {
				endingMessage += " By now you should know something about me, how much of a inspired clown I am."
			}
		}
		
		let flowEnding = FlowEnding(params: ["ID": endingTitle, "BaseString": endingMessage, "FontColor": UIColor.black, "BackgroundColor": UIColor.white])
		self.session.currentFlow = flowEnding
		self.setupCurrentFlow()
		
	}
	
}
