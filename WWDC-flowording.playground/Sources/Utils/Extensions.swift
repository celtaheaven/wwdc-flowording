import Foundation
import CoreImage
import UIKit

extension UIColor {
	public var hue: CGFloat {
		var hue: CGFloat = 0
		var saturation: CGFloat = 0
		var brightness: CGFloat = 0
		var alpha: CGFloat = 0
		
		self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
		
		return hue
	}
	
	public var coreImageColor: CIColor {
		return CIColor(color: self)
	}
}

extension CIImage {
	public var customUIImage: UIImage {
		let context = CIContext()
		let cgImage = context.createCGImage(self, from: self.extent)
		
		let uiImage = UIImage(cgImage: cgImage!)
		
		return uiImage
	}
}
