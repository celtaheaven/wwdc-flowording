import PlaygroundSupport
import UIKit

// Change the setup options according to the wanted experience.

// -- Exploration Depth (explorationDepth Argument)--
// This value refers to how many interactions is needed to finish the experience.
// It works within the minimum range of `3` up to `10`.
//
// -- Maximum Experience Time (maximumExperienceTime Argument)--
// This value refers to the maximum time, in seconds, this experience should have to reach its end.
// Default value is put to 180 seconds due to WWDC Schollarship rules.
// It works within the minimum range of `60`. Changing to `0` it'll make your experience not being limited by time (the best one, imho).

let options = SetupOptions(explorationDepth: 5,
						   maximumExperienceTime: 180)

//
// WWDC-flowording
//
// Created by Guilherme 'Heaven' Guimarães on 03/2018.
// Copyright © 2018 Guilherme 'Heaven' Guimarães. All rights reserved.
// ------------------------------------------------------
//
// -- PlaygroundPage Presentation
// Present the view controller in the Live View window
//

let controller = FlowViewController(options: options)
PlaygroundPage.current.needsIndefiniteExecution = true
PlaygroundPage.current.liveView = controller

